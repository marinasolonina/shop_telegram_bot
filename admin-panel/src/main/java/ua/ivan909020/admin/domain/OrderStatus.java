package ua.ivan909020.admin.domain;

public enum OrderStatus {

    WAITING("Ожидает"),
    PROCESSED("В процессе"),
    COMPLETED("Завершен"),
    CANCELED("Отменен");

    private final String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
