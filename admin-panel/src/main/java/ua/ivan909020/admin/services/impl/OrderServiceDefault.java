package ua.ivan909020.admin.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.admin.domain.Client;
import ua.ivan909020.admin.domain.Order;
import ua.ivan909020.admin.domain.OrderStatus;
import ua.ivan909020.admin.exceptions.ValidationException;
import ua.ivan909020.admin.repositories.OrderRepository;
import ua.ivan909020.admin.services.OrderService;
import ua.ivan909020.bot.commands.impl.OrderProductRatingCommand;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceDefault implements OrderService {

    private final OrderRepository repository;

    private final OrderProductRatingCommand orderProductRatingCommand = OrderProductRatingCommand.getInstance();

    @Autowired
    public OrderServiceDefault(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public Order findById(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("Id of Order should not be NULL");
        }
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<Order> findAll() {
        return repository.findAll();
    }

    @Override
    public Order save(Order order) {
        if (order == null) {
            throw new IllegalArgumentException("Order should not be NULL");
        }
        if (order.getId() != null) {
            throw new ValidationException("Id of Order should be NULL");
        }
        return repository.save(order);
    }

    @Override
    public Order update(Order order) {
        if (order == null) {
            throw new IllegalArgumentException("Order should not be NULL");
        }
        if (order.getId() == null) {
            throw new ValidationException("Id of Order should not be NULL");
        }

        Long chatId = findChatIdByOrderId(order.getId());

        if (order.getStatus() == OrderStatus.COMPLETED) {
            if (order.getClient().getDiscount() < 30) {
                order.getClient().setDiscount(order.getClient().getDiscount() + 5);
            }
            order.setCreatedDate(LocalDateTime.now());
        }

        if (chatId != null)
        {
            orderProductRatingCommand.execute(chatId);
        }
        else {
            throw new IllegalArgumentException("Chat id is NULL");
        }
        return repository.save(order);
    }

    @Override
    public void deleteById(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("Id of Order should not be NULL");
        }
        repository.deleteById(id);
    }

    private Long findChatIdByOrderId(Integer orderId)
    {
        Long chatId = null;
        final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
        final String username = "postgres";
        final String password = "postgres";

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;

        String query = "SELECT clients.chat_id " +
                       "FROM clients, orders " +
                       "WHERE orders.id = " + orderId + " AND orders.client_id = clients.id;";

        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                ResultSet rs = statement.executeQuery(query);
                if (rs.next()) {
                    chatId = rs.getLong(1);
                }
            } else {
                System.out.println("Failed to make connection to database");
            }
        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        return chatId;
    }
}
