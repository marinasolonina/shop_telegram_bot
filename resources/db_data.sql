insert into users(id, name, username, password, is_active, role) values
(nextval('users_id_seq'), 'Administrator', 'admin', '$2a$08$xDJ4lsYKSRLNmI6txqK2a.08NraFv2326pW/1pILqGFMvrzsnqnXu', true, 'ADMIN'),
(nextval('users_id_seq'), 'Moderator', 'moder', '$2a$08$q5k05Qx5kx0wc07SHgMxYeu1CMNkr65Fp.qhc50zavkmPi5e4.Cxu', true, 'MODER');

insert into categories(id, name) values
(nextval('categories_id_seq'), 'Пиццы'),
(nextval('categories_id_seq'), 'Бургеры'),
(nextval('categories_id_seq'), 'Суши'),
(nextval('categories_id_seq'), 'Первые блюда'),
(nextval('categories_id_seq'), 'Вторые блюда'),
(nextval('categories_id_seq'), 'Выпечка'),
(nextval('categories_id_seq'), 'Выбор клиентов');

insert into products(id, category_id, photo_url, name,  description, price) values
(nextval('products_id_seq'), 1, 'https://images.unsplash.com/photo-1565299624946-b28f40a0ae38?w=200', 'Пицца', 'Большая и вкуснейшая пицца', 19),
(nextval('products_id_seq'), 2, 'https://images.unsplash.com/photo-1568901346375-23c9450c58cd?w=200', 'Бургер', 'Бургер салатом Латук и томатами', 9),
(nextval('products_id_seq'), 5, 'https://images.unsplash.com/photo-1544025162-d76694265947?w=200', 'Ребрышки', 'Жаренные ребрышки с нарезанными томатами и картошкой', 39),
(nextval('products_id_seq'), 5, 'https://www.burgermeister.ru/images/docs//Image/beeftartar-xxl.jpg', 'Тартар говяжий', 'Тартар — это холодная закуска французской кухни', 16),
(nextval('products_id_seq'), 4, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-sup-s-plavlennym-syrom.jpg', 'Суп с плавленным сыром', 'Вкусный суп с сыром, куриным яйцом и манной крупой', 10),
(nextval('products_id_seq'), 4, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-klassicheskaya-solyanka.jpg', 'Классическая солянка', 'Классический суп с ветчиной, куриными бедрышками, сельдереем и лимоном', 16),
(nextval('products_id_seq'), 4, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-svekolnik.jpg', 'Свекольник', 'Традиционный суп из свеклы', 14),
(nextval('products_id_seq'), 4, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-dieticheskii-sup_0.jpg', 'Овощной диетический суп', 'Диетический суп с овощами. Подходит вегетарианцам', 25),
(nextval('products_id_seq'), 4, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-litovskii-kholodnyi-borshch.jpg', 'Литовский холодный борщ', 'Традиционный литовский холодник. Подходит веганам', 20),
(nextval('products_id_seq'), 5, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-lapsha-udon-s-kuritsei-i-ovoshchami.jpg', 'Лапша удон с курицей и овощами', 'Вкуснейшее блюдо из пшеничной лапши с куриной грудкой под ароматными специями', 24),
(nextval('products_id_seq'), 5, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-zhulen-iz-kuritsy.jpg', 'Жульен из курицы', 'Вкусное блюдо из курицы с сыром и грибами', 15),
(nextval('products_id_seq'), 5, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-kotlety-iz-ovoshchei.jpg', 'Котлеты из овощей', 'Вкусные овощные котлеты. Подходит веганам', 29),
(nextval('products_id_seq'), 5, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-ris-s-ovoshchami-v-multivarke.jpg', 'Рис с овощами', 'Полезное блюдо для получения энергии. Подходит вегетарианцам', 20),
(nextval('products_id_seq'), 5, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-pomidory-s-syrom-v-dukhovke.jpg', 'Помидоры с сыром в духовке', 'Запеченые в духовке помидоры с брынзой. Подходит вегетарианцам', 24),
(nextval('products_id_seq'), 5, 'https://tvoirecepty.ru/files/imagecache/recept_teaser/recept/recept-pasta-s-pomidorami.jpg', 'Паста с помидорами', 'Le bonfesto! Подходит вегетарианцам', 25),
(nextval('products_id_seq'), 6, 'https://lh3.googleusercontent.com/proxy/YiQ84dj_kaCJB6Xk35eSuhZxpApXrM4R_-GXABiTTWKhBU84M_hYUrUy3T0CeIpFZqYQSaAAan6GBmRp0fQ5dq8jJHfw_ZCUMuXz2fhS1458ShtSYw', 'Пончики', 'Сладкие пончики с глазурью в ассортименте', 5),
(nextval('products_id_seq'), 6, 'https://customs.news/wp-content/uploads/2019/11/eclair-3366430_640-e1574168185732.jpg', 'Диетические пирожные с клубникой', 'Диетические пирожные с клубникой', 7),
(nextval('products_id_seq'), 6, 'https://i.pinimg.com/originals/81/31/dc/8131dc18a18be73dc1f595d92c591089.jpg', 'Низкокалорийное печенье с вишней и цедрой апельсина', 'Ням ням', 1),
(nextval('products_id_seq'), 6, 'https://sovkusom.ru/wp-content/uploads/blog/n/nizkokaloriynaya-vypechka/1.jpg', 'Низкокалорийный пирог с яйцом', 'Большой и вкусный пирог', 20),
(nextval('products_id_seq'), 6, 'https://recept-duhovka.ru/wp-content/uploads/2020/12/retsept-keksov-v-duhovke-v-formochkah-600x400.jpg', 'Кексы', 'Кексы, посыпанные сахарной пудрой', 4),
(nextval('products_id_seq'), 3, 'https://fibonacci-food.by/image/cache/catalog/sushi/172-1280x864.JPG', 'Сет №1', 'Филадельфия, усуганасаки, иньджорсы. 8 шт. каждого', 30),
(nextval('products_id_seq'), 3, 'https://gurmans.dp.ua/giuseppe/7980-large_default/sushi-set-kaliforniya.jpg', 'Сет №2', 'Тартория, джузеппе, калифорния с ласосем, калифорния с тунцом', 33),
(nextval('products_id_seq'), 3, 'https://gurmans.dp.ua/banzay/7977-large_default/sushi-set-banzaj-xxl.jpg', 'Сет Бансай', 'Острый ролл с лососем, томатом, хрустящим огурцом, горячий ролл с двумя видами сыра и томатами', 56),
(nextval('products_id_seq'), 2, 'https://img.povar.ru/main/0f/d3/24/ae/chernii_burger-609464.jpeg', 'Черный бургер', 'Бургер из черной булки', 25),
(nextval('products_id_seq'), 2, 'https://cloud.terrapizza.by/cat/ELB2769x78809.jpg', 'Двойной чизбургер', 'Чизбургер с сыром двойной', 30),
(nextval('products_id_seq'), 1, 'https://eda.ru/img/eda/1200x-i/s2.eda.ru/StaticContent/Photos/120131085053/171027192707/p_O.jpg', 'Пицца пепперони', 'Пицца пепперони', 25),
(nextval('products_id_seq'), 1, 'https://www.green-market.by/files//catalogproducts/127-Picca_Cyplenok_teriyaki.jpg', 'Пицца с курицей и ветчиной под сыром', 'Пицца с курицей и ветчиной под сыром', 24);

insert into messages(id, name, description, text) values
(nextval('messages_id_seq'), 'START_MESSAGE', 'Начать', 'Онлайн магазин :)'),
(nextval('messages_id_seq'), 'ORDER_CREATED_MESSAGE', 'Заказ', 'Заказ оформлен. Ожидайте звонка :)'),
(nextval('messages_id_seq'), 'CART_MESSAGE', 'Корзина', '<b>Корзина</b>:

-Название: %PRODUCT_NAME%
-Описание: %PRODUCT_DESCRIPTION%

Стоимость, количесво, итого:
%PRODUCT_PRICE% BYN * %PRODUCT_QUANTITY% шт. = %PRODUCT_TOTAL_PRICE% BYN'),
(nextval('messages_id_seq'), 'PRODUCT_MESSAGE', 'Product', '<a href="%PRODUCT_PHOTO_URL%"> </a>
<b>Название</b>: %PRODUCT_NAME%
<b>Описание</b>: %PRODUCT_DESCRIPTION%
%PRODUCT_PRICES%
Стоимость, количесво, итого:
%PRODUCT_PRICE% BYN * %PRODUCT_QUANTITY% шт. = %PRODUCT_TOTAL_PRICE% BYN
%PRODUCT_PRICES%'),
(nextval('messages_id_seq'), 'ORDER_HISTORY_MESSAGE', 'История заказов', '<b>Заказ от:</b> %ORDER_DATE%
-Итоговая стоимость заказа: %ORDER_TOTAL_PRICE%'),
(nextval('messages_id_seq'), 'HISTORY_PRODUCT_MESSAGE', 'Текст продуктов', '<a href="%PRODUCT_PHOTO_URL%"> </a>
-Название: %PRODUCT_NAME%
-Описание: %PRODUCT_DESCRIPTION%

Стоимость, количесво, итого:
%PRODUCT_PRICE% BYN * %PRODUCT_QUANTITY% шт. = %PRODUCT_TOTAL_PRICE% BYN'),
(nextval('messages_id_seq'), 'FAVORITES_PRODUCT_MESSAGE', 'Любимые продукты', '<a href="%PRODUCT_PHOTO_URL%"> </a>
Вы заказывали это блюдо %COUNT% раз
<b>Название</b>: %PRODUCT_NAME%
<b>Описание</b>: %PRODUCT_DESCRIPTION%
%PRODUCT_PRICES%
Стоимость, количесво, итого:
%PRODUCT_PRICE% BYN * %PRODUCT_QUANTITY% шт. = %PRODUCT_TOTAL_PRICE% BYN
%PRODUCT_PRICES%');
