package ua.ivan909020.bot.commands;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;

import java.sql.*;
import java.util.ArrayList;

public final class Commands {

    private static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
    private static final String username = "postgres";
    private static final String password = "postgres";

    public static final String START_COMMAND = "/start";

    public static final String CATALOG_COMMAND = "\uD83D\uDCE6 Каталог";
    public static final String CART_COMMAND = "\uD83D\uDECD Корзина";
    public static final String HISTORY_COMMAND = "\uD83E\uDDFE История заказов";
    public static final String FAVORITES_COMMAND = "⭐ Любимые блюда";

    public static final String ORDER_NEXT_STEP_COMMAND = "\u2714\uFE0F Верно";
    public static final String ORDER_PREVIOUS_STEP_COMMAND = "\u25C0 Назад";
    public static final String ORDER_CANCEL_COMMAND = "\u274C Отменить заказ";

    public static final String PAYMENT_METHOD_COMMAND_CARD = "\uD83D\uDCB3 Картой";
    public static final String PAYMENT_METHOD_COMMAND_CASH = "\uD83D\uDCB5 Наличными";

    public static final String DELIVERY_METHOD_COMMAND_PICKUP = "\uD83D\uDEB6 Самовывоз";
    public static final String DELIVERY_METHOD_COMMAND_COURIER = "\uD83D\uDE9A Курьером";
    public static final String DELIVERY_METHOD_COMMAND_ORDER_A_TABLE = "\uD83C\uDF7D Заказать столик";

    public static final String PERSONS_COUNT_CORPORATE_ORDER = "Корпоративный заказ";
    public static final String PERSONS_COUNT_PERSONAL_ORDER = "Личный заказ";

    public static final String PRODUCTS_RATING_1 = "\uD83E\uDD22";
    public static final String PRODUCTS_RATING_2 = "\uD83D\uDE41";
    public static final String PRODUCTS_RATING_3 = "\uD83D\uDE10";
    public static final String PRODUCTS_RATING_4 = "\uD83D\uDE42";
    public static final String PRODUCTS_RATING_5 = "\uD83E\uDD24";

    private Commands() {
    }

    public static ReplyKeyboardMarkup createGeneralMenuKeyboard(Long chatId) {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            add(new KeyboardRow() {{
                add(CATALOG_COMMAND);
                add(CART_COMMAND);
            }});
            if (hasOrdersHistory(chatId)) {
                add(new KeyboardRow() {{
                    add(HISTORY_COMMAND);
                    if (hasFavorites(chatId)) {
                        add(FAVORITES_COMMAND);
                    }
                }});
            }
        }});
    }

    private static boolean hasOrdersHistory(Long chatId)
    {
        boolean has = false;

        ClientService clientService = ClientServiceDefault.getInstance();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                String sqlQuery = "SELECT COUNT(*) FROM orders WHERE orders.status LIKE 'COMPLETED' AND client_id = " + clientService.findByChatId(chatId).getId();

                ResultSet rs = statement.executeQuery(sqlQuery);
                if (rs.next()) {
                    if (rs.getInt(1) > 0)
                        has = true;
                }
            } else {
                System.out.println("Failed to make connection to database");
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }
        return has;
    }

    private static boolean hasFavorites(Long chatId)
    {
        boolean has = false;

        ClientService clientService = ClientServiceDefault.getInstance();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                String sqlQuery = "SELECT COUNT(product_id) " +
                                  "FROM order_items, orders " +
                                  "WHERE order_items.order_id = orders.id AND orders.client_id = " + clientService.findByChatId(chatId).getId() + " " +
                                    "AND orders.status LIKE \'COMPLETED\' " +
                                  "GROUP BY product_id " +
                                  "HAVING COUNT(product_id) > 1;";

                ResultSet rs = statement.executeQuery(sqlQuery);
                if (rs.next()) {
                    //if (rs.getInt(1) > 0)
                        has = true;
                }
            } else {
                System.out.println("Failed to make connection to database");
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        return has;
    }
}
