package ua.ivan909020.bot.commands.impl;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.domain.entities.Category;
import ua.ivan909020.bot.domain.entities.Client;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.services.CategoryService;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.CategoryServiceDefault;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CatalogCommand implements Command<Long> {

    private final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
    private final String username = "postgres";
    private final String password = "postgres";

    private static final CatalogCommand INSTANCE = new CatalogCommand();

    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final CategoryService categoryService = CategoryServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();

    private CatalogCommand() {
    }

    public static CatalogCommand getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Long chatId) {
        Client client = clientService.findByChatId(chatId);

        if (client.getName() != null) {
            if (getDateDiff(client) > 5) {
                client.setDiscount(0.0f);
                clientService.update(client);
            }
            telegramService.sendMessage(new MessageSend(chatId, "Здравствуйте, " + client.getName() + "."));
            if (client.getDiscount() > 0.0)
                telegramService.sendMessage(new MessageSend(chatId, "На этот заказ действует Ваша персональная скидка " + client.getDiscount() + "%"));
            else
                telegramService.sendMessage(new MessageSend(chatId, "На данный момент у Вас нет действующей скидки"));
        }
        telegramService.sendMessage(new MessageSend(chatId, "Выберите категорию:", createCategoriesKeyboard()));
    }

    private InlineKeyboardMarkup createCategoriesKeyboard() {
        return new InlineKeyboardMarkup().setKeyboard(new ArrayList<List<InlineKeyboardButton>>() {{
            for (Category category : categoryService.findAll()) {
                add(new ArrayList<InlineKeyboardButton>() {{
                    String categoryName = category.getName();
                    add(new InlineKeyboardButton(categoryName).setSwitchInlineQueryCurrentChat(categoryName));
                }});
            }
        }});
    }

    private long getDateDiff(Client client)
    {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        LocalTime lastDate = LocalTime.now();

        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                String sqlQuery = "Select max(created_date) From orders, clients WHERE orders.status LIKE 'COMPLETED' AND clients.id = " + client.getId();

                ResultSet rs = statement.executeQuery(sqlQuery);
                if (rs.next()) {
                    lastDate = rs.getTime(1).toLocalTime();
                }
            } else {
                System.out.println("Failed to make connection to database");
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        LocalTime CurrentDate = LocalTime.now();

        return Math.abs(CurrentDate.getMinute() - lastDate.getMinute());
    }
}
