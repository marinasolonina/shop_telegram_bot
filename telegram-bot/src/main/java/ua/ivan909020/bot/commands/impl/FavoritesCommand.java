package ua.ivan909020.bot.commands.impl;

import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.domain.entities.Client;
import ua.ivan909020.bot.domain.entities.Message;
import ua.ivan909020.bot.domain.entities.Product;
import ua.ivan909020.bot.domain.models.CartItem;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.MessageService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.MessageServiceCached;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ua.ivan909020.bot.domain.models.MessagePlaceholder.of;

public class FavoritesCommand implements Command<Long> {

    private class FavoriteCount
    {
        private int count;
        private Product product;

        public FavoriteCount() {}

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }
    }

    private static final FavoritesCommand INSTANCE = new FavoritesCommand();

    private final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
    private final String username = "postgres";
    private final String password = "postgres";

    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();
    private final MessageService messageService = MessageServiceCached.getInstance();

    public FavoritesCommand() {}

    public static FavoritesCommand getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Long chatId) {
        Client client = clientService.findByChatId(chatId);

        telegramService.sendMessage(new MessageSend(chatId, "Ваши любимые блюда"));

        ArrayList<FavoriteCount> favoriteProducts = getAllFavoriteProducts(client);
        for (FavoriteCount product : favoriteProducts)
        {
            telegramService.sendMessage(new MessageSend(chatId, createProductText(product)));
        }
    }

    private String createProductText(FavoriteCount fav) {
        Message message = messageService.findByName("FAVORITES_PRODUCT_MESSAGE");
        if (fav != null) {
            message.applyPlaceholder(of("%COUNT%", fav.getCount()));
            message.applyPlaceholder(of("%PRODUCT_PHOTO_URL%", fav.getProduct().getPhotoUrl()));
            message.applyPlaceholder(of("%PRODUCT_NAME%", fav.getProduct().getName()));
            message.applyPlaceholder(of("%PRODUCT_DESCRIPTION%", fav.getProduct().getDescription()));
            message.applyPlaceholder(of("%PRODUCT_PRICE%", fav.getProduct().getPrice()));
        }
        return message.buildText();
    }

    private ArrayList<FavoriteCount> getAllFavoriteProducts(Client client)
    {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;
        ArrayList<FavoriteCount> list = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                String sqlQuery = "SELECT COUNT(products.id), products.id, products.photo_url, products.name, products.description, products.price " +
                        "FROM orders, products, order_items " +
                        "WHERE orders.client_id = " + client.getId() + " AND orders.status LIKE \'COMPLETED\' " +
                          "AND order_items.product_id = products.id AND order_items.order_id = orders.id " +
                        "GROUP BY products.id " +
                        "HAVING COUNT(products.id) > 1 " +
                        "ORDER BY COUNT(products.id) DESC " +
                        "FETCH FIRST 3 ROWS ONLY;";

                ResultSet rs = statement.executeQuery(sqlQuery);
                while (rs.next()) {
                    FavoriteCount favoriteCount = new FavoriteCount();
                    favoriteCount.setCount(rs.getInt(1));

                    Product product = new Product();
                    product.setId(rs.getInt(2));
                    product.setPhotoUrl(rs.getString(3));
                    product.setName(rs.getString(4));
                    product.setDescription(rs.getString(5));
                    product.setPrice(rs.getFloat(6));
                    favoriteCount.setProduct(product);

                    list.add(favoriteCount);
                }
            } else {
                System.out.println("Failed to make connection to database");
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        return list;
    }
}
