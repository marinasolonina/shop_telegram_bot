package ua.ivan909020.bot.commands.impl;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Order;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.exceptions.OrderStepStateException;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderStepService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.OrderStepServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;

import java.util.ArrayList;

public class OrderChooseDeliveryMethodCommand implements Command<Long> {

    private static final OrderChooseDeliveryMethodCommand INSTANCE = new OrderChooseDeliveryMethodCommand();

    private final OrderStepService orderStepService = OrderStepServiceDefault.getInstance();
    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();

    public OrderChooseDeliveryMethodCommand()
    {}

    public static OrderChooseDeliveryMethodCommand getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Long chatId) {
        clientService.setActionForChatId(chatId, "order=enter-delivery-method");
        telegramService.sendMessage(new MessageSend(chatId, "Выберите способ доставки", createKeyboard(false)));
    }

    private ReplyKeyboardMarkup createKeyboard(boolean skipStep) {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            if (skipStep) {
                add(new KeyboardRow() {{
                    add(new KeyboardButton(Commands.ORDER_NEXT_STEP_COMMAND));
                }});
            }
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.DELIVERY_METHOD_COMMAND_PICKUP));
                add(new KeyboardButton(Commands.DELIVERY_METHOD_COMMAND_COURIER));
            }});
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.DELIVERY_METHOD_COMMAND_ORDER_A_TABLE));
            }});
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.ORDER_CANCEL_COMMAND));
                add(new KeyboardButton(Commands.ORDER_PREVIOUS_STEP_COMMAND));
            }});
        }});
    }

    public void doChooseDeliveryMethod(Long chatId, String method) {
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
        order.setDeliveryMethod(method);
        orderStepService.updateCachedOrder(chatId, order);

        if (method.equals(Commands.DELIVERY_METHOD_COMMAND_COURIER)) {
            orderStepService.skipPersonsCount(chatId);
        }
        else if (method.equals(Commands.DELIVERY_METHOD_COMMAND_ORDER_A_TABLE)) {
            orderStepService.nextOrderStep(chatId);
        } else {
            orderStepService.skipAll(chatId);
        }
    }
}
