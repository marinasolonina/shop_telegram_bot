package ua.ivan909020.bot.commands.impl;

import com.mchange.v2.lang.StringUtils;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Client;
import ua.ivan909020.bot.domain.entities.Order;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.exceptions.OrderStepStateException;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderStepService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.OrderStepServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;

import java.util.ArrayList;

public class OrderChoosePaymentMethod implements Command<Long> {

    private static final OrderChoosePaymentMethod INSTANCE = new OrderChoosePaymentMethod();

    private final OrderStepService orderStepService = OrderStepServiceDefault.getInstance();
    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();

    public OrderChoosePaymentMethod() {
    }

    public static OrderChoosePaymentMethod getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Long chatId) {
        clientService.setActionForChatId(chatId, "order=enter-payment-method");
        //Order order = orderStepService.findCachedOrderByChatId(chatId);
        telegramService.sendMessage(new MessageSend(chatId, "Выберите способ оплаты", createKeyboard(false)));
        sendCurrentPaymentMethod(chatId);
    }

    private ReplyKeyboardMarkup createKeyboard(boolean skipStep) {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            if (skipStep) {
                add(new KeyboardRow() {{
                    add(new KeyboardButton(Commands.ORDER_NEXT_STEP_COMMAND));
                }});
            }
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.PAYMENT_METHOD_COMMAND_CASH));
                add(new KeyboardButton(Commands.PAYMENT_METHOD_COMMAND_CARD));
            }});
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.ORDER_CANCEL_COMMAND));
                add(new KeyboardButton(Commands.ORDER_PREVIOUS_STEP_COMMAND));
            }});
        }});
    }

    private void sendCurrentPaymentMethod(Long chatId) {
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
        if (StringUtils.nonWhitespaceString(order.getPaymentMethod())) {
            telegramService.sendMessage(new MessageSend(chatId,
                    "Оплата: " + order.getPaymentMethod(), createKeyboard(true)));
        }
    }

    public void doChoosePaymentMethod(Long chatId, String method) {
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
        order.setPaymentMethod(method);
        orderStepService.updateCachedOrder(chatId, order);
        orderStepService.nextOrderStep(chatId);
    }

}
