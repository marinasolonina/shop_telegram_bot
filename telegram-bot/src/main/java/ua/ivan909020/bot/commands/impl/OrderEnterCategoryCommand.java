package ua.ivan909020.bot.commands.impl;


import java.util.ArrayList;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Order;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.exceptions.OrderStepStateException;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderService;
import ua.ivan909020.bot.services.OrderStepService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.OrderServiceDefault;
import ua.ivan909020.bot.services.impl.OrderStepServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;


public class OrderEnterCategoryCommand implements Command<Long>
{

    private static final OrderEnterCategoryCommand INSTANCE = new OrderEnterCategoryCommand();

    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();
    private final OrderStepService orderStepService = OrderStepServiceDefault.getInstance();

    private OrderEnterCategoryCommand(){
    }

    public static OrderEnterCategoryCommand getInstance(){ return INSTANCE; }

    @Override
    public void execute(Long chatId)
    {
        clientService.setActionForChatId(chatId, "order=enter-category");
        telegramService.sendMessage(new MessageSend(chatId, "Выберите категорию для бронирования", createKeyboard(false)));
        sendCurrentCount(chatId);
    }



    private void sendCurrentCount(Long chatId)
    {
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
    }

    private ReplyKeyboardMarkup createKeyboard(boolean skipStep)
    {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            if (skipStep) {
                add(new KeyboardRow() {{
                    add(new KeyboardButton(Commands.ORDER_NEXT_STEP_COMMAND));
                }});
            }
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.PERSONS_COUNT_PERSONAL_ORDER));
                add(new KeyboardButton(Commands.PERSONS_COUNT_CORPORATE_ORDER));
            }});
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.ORDER_CANCEL_COMMAND));
                add(new KeyboardButton(Commands.ORDER_PREVIOUS_STEP_COMMAND));
            }});
        }});
    }

    public void doEnterCategory(Long chatId, String method) {
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
        OrderServiceDefault.setCATEGORY(method);
        orderStepService.nextOrderStep(chatId);
    }
}
