package ua.ivan909020.bot.commands.impl;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Order;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.exceptions.OrderStepStateException;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderStepService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.OrderServiceDefault;
import ua.ivan909020.bot.services.impl.OrderStepServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;


public class OrderEnterPersonsCountCommand implements Command<Long>
{
    private static final OrderEnterPersonsCountCommand INSTANCE = new OrderEnterPersonsCountCommand();

    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();
    private final OrderStepService orderStepService = OrderStepServiceDefault.getInstance();

    private static final Pattern COUNT_PATTERN = Pattern.compile("[0-9]");

    private OrderEnterPersonsCountCommand() {
    }

    public static OrderEnterPersonsCountCommand getInstance(){
        return INSTANCE;
    }

    @Override
    public void execute(Long chatId)
    {
        clientService.setActionForChatId(chatId, "order=enter-client-count-persons");
        telegramService.sendMessage(new MessageSend(chatId, "Введите количество необходимых посадочных мест", createKeyboard(false)));
        sendCurrentCount(chatId);
    }

    private void sendCurrentCount(Long chatId)
    {
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
    }

    private ReplyKeyboardMarkup createKeyboard(boolean skipStep)
    {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            if (skipStep) {
                add(new KeyboardRow() {{
                    add(new KeyboardButton(Commands.ORDER_NEXT_STEP_COMMAND));
                }});
            }
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.ORDER_CANCEL_COMMAND));
                add(new KeyboardButton(Commands.ORDER_PREVIOUS_STEP_COMMAND));
            }});
        }});
    }

    public void doEnterPersonsCount(Long chatId, String count) {
        Matcher matcher = COUNT_PATTERN.matcher(count);
        if (!matcher.find()) {
            telegramService.sendMessage(new MessageSend(chatId, "Вы ввели некорректное значение, попробуйте снова."));
            return;
        }
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }

        if (Integer.parseInt(count) > 15 && OrderServiceDefault.getCATEGORY().equals(Commands.PERSONS_COUNT_PERSONAL_ORDER)){
            telegramService.sendMessage(new MessageSend(chatId, "Извините, мы не можем зарезервировать больше 15 мест для личного заказа"));
            return;
        }
        else if ( Integer.parseInt(count) < 16 && OrderServiceDefault.getCATEGORY().equals(Commands.PERSONS_COUNT_CORPORATE_ORDER)){
            telegramService.sendMessage(new MessageSend(chatId, "Извините, корпоративный заказ мы можем оформить только от 16 персон"));
            return;
        }
        else if (Integer.parseInt(count) > 65 && OrderServiceDefault.getCATEGORY().equals(Commands.PERSONS_COUNT_CORPORATE_ORDER)){
            telegramService.sendMessage(new MessageSend(chatId, "Извините, мы не можем зарезервировать больше 65 мест для корпоративного заказа"));
            return;
        }
        else {
            order.setPersonsCount(Integer.parseInt(count));
            orderStepService.updateCachedOrder(chatId, order);
            orderStepService.skipAddress(chatId);
        }
    }
}
