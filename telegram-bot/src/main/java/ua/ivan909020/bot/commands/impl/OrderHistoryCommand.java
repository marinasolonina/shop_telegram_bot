package ua.ivan909020.bot.commands.impl;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.domain.entities.*;
import ua.ivan909020.bot.domain.models.CartItem;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.domain.models.OrderHistory;
import ua.ivan909020.bot.services.*;
import ua.ivan909020.bot.services.impl.*;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ua.ivan909020.bot.domain.models.MessagePlaceholder.of;

public class OrderHistoryCommand implements Command<Long> {

    private static final OrderHistoryCommand INSTANCE = new OrderHistoryCommand();
    private ArrayList<OrderHistory> ordersInfo;

    private final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
    private final String username = "postgres";
    private final String password = "postgres";

    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();
    private final MessageService messageService = MessageServiceCached.getInstance();

    public OrderHistoryCommand()
    {}

    public static OrderHistoryCommand getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Long chatId) {
        Client client = clientService.findByChatId(chatId);

        telegramService.sendMessage(new MessageSend(chatId, "История Ваших заказов"));

        ordersInfo = getAllOrders(client);
        for (OrderHistory order : ordersInfo)
        {
            telegramService.sendMessage(new MessageSend(chatId, createOrderText(order)));
            for (CartItem cartItem : order.getCartItems())
            {
                telegramService.sendMessage(new MessageSend(chatId, createProductText(cartItem)));
            }
        }
    }

    private String createOrderText(OrderHistory orderHistory) {
        Message message = messageService.findByName("ORDER_HISTORY_MESSAGE");
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Date createdDate = java.sql.Timestamp.valueOf(orderHistory.getOrderItem().getOrder().getCreatedDate());
        if (orderHistory != null) {
            message.applyPlaceholder(of("%ORDER_DATE%", sdf.format(createdDate)));
            message.applyPlaceholder(of("%ORDER_TOTAL_PRICE%", orderHistory.getOrderItem().getOrder().getAmount()));
        }
        return message.buildText();
    }

    private String createProductText(CartItem cartItem) {
        Message message = messageService.findByName("HISTORY_PRODUCT_MESSAGE");
        if (cartItem != null) {
            message.applyPlaceholder(of("%PRODUCT_PHOTO_URL%", cartItem.getProduct().getPhotoUrl()));
            message.applyPlaceholder(of("%PRODUCT_CATEGORY%", cartItem.getProduct().getCategory().getName()));
            message.applyPlaceholder(of("%PRODUCT_NAME%", cartItem.getProduct().getName()));
            message.applyPlaceholder(of("%PRODUCT_DESCRIPTION%", cartItem.getProduct().getDescription()));
            message.applyPlaceholder(of("%PRODUCT_PRICE%", cartItem.getProduct().getPrice()));
            message.applyPlaceholder(of("%PRODUCT_QUANTITY%", cartItem.getQuantity()));
            message.applyPlaceholder(of("%PRODUCT_TOTAL_PRICE%", cartItem.getProduct().getPrice() * cartItem.getQuantity()));
        }
        return message.buildText();
    }

    private InlineKeyboardMarkup createCartKeyboard(int currentCartPage, List<CartItem> cartItems) {
        return new InlineKeyboardMarkup().setKeyboard(new ArrayList<List<InlineKeyboardButton>>() {{
            add(new ArrayList<InlineKeyboardButton>() {{
                add(new InlineKeyboardButton("\u25c0").setCallbackData("history=previous-product"));
                add(new InlineKeyboardButton((currentCartPage + 1) + "/" + cartItems.size())
                        .setCallbackData("history=current-page"));
                add(new InlineKeyboardButton("\u25b6").setCallbackData("history=next-product"));
            }});
        }});
    }

    private ArrayList<OrderHistory> getAllOrders(Client client)
    {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;

        String query = "SELECT orders.id, categories.name, order_items.quantity, " +
                              "orders.created_date, orders.amount, " +
                              "products.photo_url, products.name, products.description, products.price " +
                       "FROM orders, order_items, products, categories " +
                       "WHERE order_items.order_id = orders.id " +
                             "AND order_items.product_id = products.id " +
                             "AND products.category_id = categories.id " +
                             "AND orders.status LIKE \'COMPLETED\' " +
                             "AND orders.client_id = " + client.getId() + " " +
                       "ORDER BY orders.created_date;";

        ArrayList<OrderHistory> ordersSet = new ArrayList<OrderHistory>();

        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                ResultSet rs = statement.executeQuery(query);
                int i = -1;
                while (rs.next()) {
                    OrderHistory oh = new OrderHistory();
                    oh.setOrderId(rs.getInt(1));

                    Category category = new Category();
                    category.setName(rs.getString(2));
                    OrderItem orderItem = new OrderItem();
                    orderItem.setQuantity(rs.getInt(3));
                    Order order = new Order();
                    LocalDateTime ldt = rs.getTimestamp(4).toLocalDateTime();
                    order.setCreatedDate(ldt);
                    order.setAmount(rs.getFloat(5));
                    orderItem.setOrder(order);
                    Product product = new Product();
                    product.setPhotoUrl(rs.getString(6));
                    product.setName(rs.getString(7));
                    product.setDescription(rs.getString(8));
                    product.setPrice(rs.getFloat(9));
                    product.setCategory(category);
                    orderItem.setProduct(product);

                    CartItem cartItem = new CartItem();
                    cartItem.setProduct(product);
                    cartItem.setQuantity(orderItem.getQuantity());

                    oh.setCategory(category);
                    oh.setOrderItem(orderItem);
                    oh.getCartItems().add(cartItem);

                    if (ordersSet.size() > 0) {
                        if (ordersSet.get(i).getOrderId() == oh.getOrderId()) {
                            ordersSet.get(i).unite(oh);
                        }
                        else {
                            ordersSet.add(oh);
                            i++;
                        }
                    } else {
                        ordersSet.add(oh);
                        i++;
                    }

                }
            } else {
                System.out.println("Failed to make connection to database");
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        return ordersSet;
    }
}
