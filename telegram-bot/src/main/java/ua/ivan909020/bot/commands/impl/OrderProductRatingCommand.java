package ua.ivan909020.bot.commands.impl;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Category;
import ua.ivan909020.bot.domain.entities.Product;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.MessageService;
import ua.ivan909020.bot.services.OrderStepService;
import ua.ivan909020.bot.services.ProductService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.MessageServiceCached;
import ua.ivan909020.bot.services.impl.OrderStepServiceDefault;
import ua.ivan909020.bot.services.impl.ProductServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;



public class OrderProductRatingCommand implements Command<Long>
{
    private static final OrderProductRatingCommand INSTANCE = new OrderProductRatingCommand();
    private static Integer productId = null;

    private final TelegramService telegramService = TelegramServiceDefault.getInstance();
    private final ClientService clientService = ClientServiceDefault.getInstance();
    private final OrderStepService orderStepService = OrderStepServiceDefault.getInstance();
    private final MessageService messageService = MessageServiceCached.getInstance();
    private final ProductService productService = ProductServiceDefault.getInstance();


    public OrderProductRatingCommand()
    {}


    public static OrderProductRatingCommand getInstance()
    {
        return INSTANCE;
    }


    public static Integer getProductId()
    {
        return productId;
    }

    public static void setProductId(Integer productId)
    {
        OrderProductRatingCommand.productId = productId;
    }


    @Override
    public void execute(Long chatId)
    {
        clientService.setActionForChatId(chatId, "order=rate");
        telegramService.sendMessage(new MessageSend(chatId, "Пожалуйста, поделитесь впечатлениями о блюдах, которые Вы заказали", createYesOrNo()));
    }

    private ReplyKeyboardMarkup createYesOrNo()
    {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            add(new KeyboardRow() {{
                add(new KeyboardButton("Оставить оценку"));
                add(new KeyboardButton("Отмена"));
            }});
        }});
    }

    private ReplyKeyboardMarkup createKeyboard(boolean skipStep) {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            if (skipStep) {
                add(new KeyboardRow() {{
                    add(new KeyboardButton(Commands.ORDER_NEXT_STEP_COMMAND));
                }});
            }
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.PRODUCTS_RATING_1));
                add(new KeyboardButton(Commands.PRODUCTS_RATING_2));
                add(new KeyboardButton(Commands.PRODUCTS_RATING_3));
                add(new KeyboardButton(Commands.PRODUCTS_RATING_4));
                add(new KeyboardButton(Commands.PRODUCTS_RATING_5));
            }});
        }});
    }

    private String createProductText(Product product) {
        return "<a href=\"" + product.getPhotoUrl() + "\"> </a>\n"
                + "-Категория: " + product.getCategory().getName() + "\n"
                + "-Название: " + product.getName();
    }

    public void doYes(Long chatId)
    {
        clientService.setActionForChatId(chatId, "order=rate-yes");
        ArrayList<Product> productsList = findAllProductsInTheOrder(chatId);
        telegramService.sendMessage(new MessageSend(chatId, "Выберите блюдо на оценку из последнего заказа", createProductsKeyboard(productsList)));
    }

    public void doNo(Long chatId)
    {
        telegramService.sendMessage(new MessageSend(chatId, "Спасибо за заказ :)", Commands.createGeneralMenuKeyboard(chatId)));
    }

    private InlineKeyboardMarkup createProductsKeyboard(ArrayList<Product> productsList) {
        return new InlineKeyboardMarkup().setKeyboard(new ArrayList<List<InlineKeyboardButton>>() {{
            for (Product product : productsList) {
                add(new ArrayList<InlineKeyboardButton>() {{
                    String productName = product.getName();
                    add(new InlineKeyboardButton(productName).setSwitchInlineQueryCurrentChat(productName));
                }});
            }
        }});
    }

    public void doEnterProductRate(Long chatId, String text)
    {
        if (chatId == null)
        {
            throw new IllegalArgumentException("Chat id should not be NULL");
        }
        if (OrderProductRatingCommand.productId == null)
        {
            throw new IllegalArgumentException("Product Id should not be NULL");
        }
        Product product = productService.findById(productId);
        Float rate = product.getRating();
        if (rate != null)
        {
            rate = (rate + getRate(text)) / 2;
        }
        else
        {
            rate = Float.valueOf(getRate(text));
        }

        product.setRating(rate);
        productService.update(product);
        ArrayList<Product> products = findAllProductsInTheOrder(chatId);
        if (products.size() > 1)
        {
            telegramService.sendMessage(new MessageSend(chatId, "Желаете оценить остальные блюда?", createYesOrNo()));
        }
        else {
            telegramService.sendMessage(new MessageSend(chatId, "Спасибо за оценку! :)", Commands.createGeneralMenuKeyboard(chatId)));
        }
    }

    private int getRate(String rate)
    {
        int ret = 0;
        switch (rate)
        {
            case Commands.PRODUCTS_RATING_1:
                ret = 1;
                break;
            case Commands.PRODUCTS_RATING_2:
                ret = 2;
                break;
            case Commands.PRODUCTS_RATING_3:
                ret = 3;
                break;
            case Commands.PRODUCTS_RATING_4:
                ret = 4;
                break;
            case Commands.PRODUCTS_RATING_5:
                ret = 5;
                break;
        }
        return ret;
    }

    public void showProduct(InlineQuery inlineQuery)
    {
        Long chatId = inlineQuery.getFrom().getId().longValue();
        String inlineQueryId = inlineQuery.getId();
        String productName = inlineQuery.getQuery();

        ArrayList<Product> products = findAllProductsInTheOrder(chatId);
        for (Product p : products)
        {
            if (p.getName().equals(productName))
            {
                OrderProductRatingCommand.setProductId(p.getId());
                telegramService.sendMessage(new MessageSend(chatId, createProductText(p), createKeyboard(false)));
                break;
            }
        }
    }

    private ArrayList<Product> findAllProductsInTheOrder(Long chatId)
    {
        final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
        final String username = "postgres";
        final String password = "postgres";

        ArrayList<Product> products = new ArrayList<>();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;

        String query = "SELECT products.id, categories.id, categories.name, products.photo_url, products.name "
                + "FROM orders, categories, products, order_items "
                + "WHERE products.category_id = categories.id AND products.id = order_items.product_id "
                   + "AND orders.id = order_items.order_id "
                   + "AND orders.id = (SELECT MAX(orders.id) FROM orders) "
                   + "AND orders.client_id = (SELECT clients.id FROM clients WHERE clients.chat_id = " + chatId + ")";

        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    Product product = new Product();
                    product.setId(rs.getInt(1));
                    Category category = new Category();
                    category.setId(rs.getInt(2));
                    category.setName(rs.getString(3));
                    product.setCategory(category);
                    product.setPhotoUrl(rs.getString(4));
                    product.setName(rs.getString(5));

                    products.add(product);
                }
            } else {
                System.out.println("Failed to make connection to database");
            }
        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        return products;
    }
}
