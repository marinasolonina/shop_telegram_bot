package ua.ivan909020.bot.commands.impl;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Client;
import ua.ivan909020.bot.domain.entities.Order;
import ua.ivan909020.bot.domain.models.CartItem;
import ua.ivan909020.bot.domain.models.MessageEdit;
import ua.ivan909020.bot.domain.models.MessageSend;
import ua.ivan909020.bot.exceptions.OrderStepStateException;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderService;
import ua.ivan909020.bot.services.OrderStepService;
import ua.ivan909020.bot.services.TelegramService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.OrderServiceDefault;
import ua.ivan909020.bot.services.impl.OrderStepServiceDefault;
import ua.ivan909020.bot.services.impl.TelegramServiceDefault;
import ua.ivan909020.bot.utils.KeyboardUtils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class SetTimeCommand implements Command<Long> {

    private TelegramService telegramService = TelegramServiceDefault.getInstance();
    private ClientService clientService = ClientServiceDefault.getInstance();
    private OrderService orderService = OrderServiceDefault.getInstance();
    private OrderStepService orderStepService = OrderStepServiceDefault.getInstance();

    private static final SetTimeCommand INSTANCE = new SetTimeCommand();
    private static int hours = LocalTime.now().getHour();
    private static int minutes = LocalTime.now().getMinute();

    public SetTimeCommand() {}

    public static SetTimeCommand getInstance() {
        return INSTANCE;
    }

    public static int getHours() {
        return hours;
    }

    public static void setHours(int hours) {
        SetTimeCommand.hours = hours;
    }

    public static int getMinutes() {
        return minutes;
    }

    public static void setMinutes(int minutes) {
        SetTimeCommand.minutes = minutes;
    }

    @Override
    public void execute(Long chatId) {
        clientService.setActionForChatId(chatId, "time=set-time");
        telegramService.sendMessage(new MessageSend(chatId, "Выберите удобное для Вас время, чтобы получить заказ", createTimePicker(true)));
        telegramService.sendMessage(new MessageSend(chatId, "Назначьте время", createKeyboard(false)));
        //createKeyboard(false);
    }

    private ReplyKeyboardMarkup createKeyboard(boolean skipStep) {
        return KeyboardUtils.create(new ArrayList<KeyboardRow>() {{
            if (skipStep) {
                add(new KeyboardRow() {{
                    add(new KeyboardButton(Commands.ORDER_NEXT_STEP_COMMAND));
                }});
            }
            add(new KeyboardRow() {{
                add(new KeyboardButton("Назначить"));
                add(new KeyboardButton("Не назначать"));
            }});
            add(new KeyboardRow() {{
                add(new KeyboardButton(Commands.ORDER_CANCEL_COMMAND));
                add(new KeyboardButton(Commands.ORDER_PREVIOUS_STEP_COMMAND));
            }});
        }});
    }

    private InlineKeyboardMarkup createTimePicker(boolean setNew) {
        if (setNew) {
            hours = LocalTime.now().getHour();
            minutes = LocalTime.now().getMinute();
        }
        return new InlineKeyboardMarkup().setKeyboard(new ArrayList<List<InlineKeyboardButton>>() {{
            add(new ArrayList<InlineKeyboardButton>() {{
                add(new InlineKeyboardButton("⬆").setCallbackData("time-picker=inc-hours"));
                add(new InlineKeyboardButton("⬆").setCallbackData("time-picker=inc-minutes"));
            }});
            add(new ArrayList<InlineKeyboardButton>() {{
                add(new InlineKeyboardButton(String.valueOf(hours)).setCallbackData("time-piker=hours-field"));
                add(new InlineKeyboardButton(String.valueOf(minutes)).setCallbackData("time-piker=minutes-field"));
            }});
            add(new ArrayList<InlineKeyboardButton>() {{
                add(new InlineKeyboardButton("⬇").setCallbackData("time-picker=dec-hours"));
                add(new InlineKeyboardButton("⬇").setCallbackData("time-picker=dec-minutes"));
            }});
        }});
    }

    public void doIncreaseHours(Long chatId, Integer messageId)
    {
        hours++;
        if (hours == 24)
        {
            hours = 0;
        }
        telegramService.editMessageText(new MessageEdit(chatId, messageId, "Выберите удобное для Вас время, чтобы получить заказ", createTimePicker(false)));
    }

    public void doDecreaseHours(Long chatId, Integer messageId)
    {
        hours--;
        if (hours == -1)
        {
            hours = 23;
        }
        telegramService.editMessageText(new MessageEdit(chatId, messageId, "Выберите удобное для Вас время, чтобы получить заказ", createTimePicker(false)));
    }

    public void doIncreaseMinutes(Long chatId, Integer messageId)
    {
        minutes++;
        if (minutes == 60)
        {
            minutes = 0;
        }
        telegramService.editMessageText(new MessageEdit(chatId, messageId, "Выберите удобное для Вас время, чтобы получить заказ", createTimePicker(false)));
    }

    public void doDecreaseMinutes(Long chatId, Integer messageId)
    {
        minutes--;
        if (minutes == -1)
        {
            minutes = 59;
        }
        telegramService.editMessageText(new MessageEdit(chatId, messageId, "Выберите удобное для Вас время, чтобы получить заказ", createTimePicker(false)));
    }

    public void doSetTime(Long chatId, String message)
    {
        String expectedTime = "Не назначено";
        if (message.equals("Назначить")) {
            expectedTime = hours + ":" + minutes;
        }
        Order order = orderStepService.findCachedOrderByChatId(chatId);
        if (order == null || order.getClient() == null) {
            throw new OrderStepStateException("Order step state error for client with chatId" + chatId);
        }
        order.setExpectedTime(expectedTime);
        orderStepService.updateCachedOrder(chatId, order);
        orderStepService.nextOrderStep(chatId);
    }
}

