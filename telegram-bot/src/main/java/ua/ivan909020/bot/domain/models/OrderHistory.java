package ua.ivan909020.bot.domain.models;

import ua.ivan909020.bot.domain.entities.Category;
import ua.ivan909020.bot.domain.entities.OrderItem;

import java.util.ArrayList;

public class OrderHistory {
    private int orderId;
    private Category category;
    private OrderItem orderItem;
    private ArrayList<CartItem> cartItems;

    public OrderHistory() {
        cartItems = new ArrayList<>();
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public ArrayList<CartItem> getCartItems() {
        return cartItems;
    }

    public void unite(OrderHistory orderHistory)
    {
        this.cartItems.addAll(orderHistory.cartItems);
    }
}
