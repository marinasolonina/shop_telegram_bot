package ua.ivan909020.bot.handlers.impl;

import java.nio.charset.StandardCharsets;

import org.telegram.telegrambots.meta.api.objects.Message;
import ua.ivan909020.bot.commands.impl.*;
import ua.ivan909020.bot.handlers.Handler;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderService;
import ua.ivan909020.bot.services.impl.ClientServiceDefault;
import ua.ivan909020.bot.services.impl.OrderServiceDefault;

class ActionHandler implements Handler<Message> {

    private final ClientService clientService = ClientServiceDefault.getInstance();
    private final OrderService orderService = OrderServiceDefault.getInstance();

    private final OrderEnterNameCommand enterNameCommand = OrderEnterNameCommand.getInstance();
    private final OrderEnterPhoneNumberCommand enterPhoneNumberCommand = OrderEnterPhoneNumberCommand.getInstance();
    private final OrderEnterCityCommand enterCityNameCommand = OrderEnterCityCommand.getInstance();
    private final OrderEnterAddressCommand orderEnterAddressCommand = OrderEnterAddressCommand.getInstance();
    private final OrderChoosePaymentMethod orderChoosePaymentMethod = OrderChoosePaymentMethod.getInstance();
    private final OrderChooseDeliveryMethodCommand orderChooseDeliveryMethodCommand = OrderChooseDeliveryMethodCommand.getInstance();
    private final OrderEnterPersonsCountCommand orderEnterPersonsCountCommand = OrderEnterPersonsCountCommand.getInstance();
    private final OrderEnterCategoryCommand orderEnterCategoryCommand = OrderEnterCategoryCommand.getInstance();
    private final SetTimeCommand setTimeCommand = SetTimeCommand.getInstance();
    private final OrderProductRatingCommand orderProductRatingCommand = OrderProductRatingCommand.getInstance();

    @Override
    public void handle(Message message) {
        Long chatId = message.getChatId();
        String text = message.getText();
        String action = clientService.findActionByChatId(chatId);

        if ("order=enter-client-name".equals(action)) {
            enterNameCommand.doEnterName(chatId, text);
        } else if ("order=enter-client-phone-number".equals(action)) {
            enterPhoneNumberCommand.doEnterPhoneNumber(chatId, text);
        } else if ("order=enter-client-city".equals(action)) {
            enterCityNameCommand.doEnterCity(chatId, text);
        } else if ("order=enter-client-address".equals(action)) {
            orderEnterAddressCommand.doEnterAddress(chatId, text);
        } else if ("order=enter-payment-method".equals(action)) {
            orderChoosePaymentMethod.doChoosePaymentMethod(chatId, text);
        } else if ("order=enter-delivery-method".equals(action)) {
            orderChooseDeliveryMethodCommand.doChooseDeliveryMethod(chatId, text);
        } else if ("order=enter-client-count-persons".equals(action)) {
            orderEnterPersonsCountCommand.doEnterPersonsCount(chatId, text);
        } else if ("order=enter-category".equals(action)) {
            orderEnterCategoryCommand.doEnterCategory(chatId, text);
        } else if ("time=set-time".equals(action)) {
            setTimeCommand.doSetTime(chatId, text);
        } else if ("order=rate".equals(action)) {
            if (text.equals("Оставить оценку"))
            {
                orderProductRatingCommand.doYes(chatId);
            }
            else
            {
                orderProductRatingCommand.doNo(chatId);
            }
        } else if ("order=rate-yes".equals(action)) {
            if (text.equals("Отмена"))
            {
                orderProductRatingCommand.doNo(chatId);
            }
            else
            {
                orderProductRatingCommand.doEnterProductRate(chatId, text);
            }
        }
    }

}
