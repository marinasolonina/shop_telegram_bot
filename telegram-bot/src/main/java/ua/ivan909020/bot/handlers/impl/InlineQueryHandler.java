package ua.ivan909020.bot.handlers.impl;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import ua.ivan909020.bot.commands.impl.OrderProductRatingCommand;
import ua.ivan909020.bot.commands.impl.ShowProductsCommand;
import ua.ivan909020.bot.domain.entities.Category;
import ua.ivan909020.bot.domain.entities.Product;
import ua.ivan909020.bot.handlers.Handler;

class InlineQueryHandler implements Handler<InlineQuery> {

    private final ShowProductsCommand showProductsCommand = ShowProductsCommand.getInstance();
    private final OrderProductRatingCommand orderProductRatingCommand = OrderProductRatingCommand.getInstance();

    @Override
    public void handle(InlineQuery inlineQuery) {
        if (inlineQuery.hasQuery())
        {
            if (getAllCategories().contains(inlineQuery.getQuery()))
            {
                showProductsCommand.execute(inlineQuery);
            }
            else
            {
                orderProductRatingCommand.showProduct(inlineQuery);
            }
        }
    }

    private ArrayList<String> getAllCategories()
    {
        final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
        final String username = "postgres";
        final String password = "postgres";

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;

        String query = "SELECT name FROM categories";
        ArrayList<String> categories = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    categories.add(rs.getString(1));
                }
            } else {
                System.out.println("Failed to make connection to database");
            }
        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }

        return categories;
    }
}
