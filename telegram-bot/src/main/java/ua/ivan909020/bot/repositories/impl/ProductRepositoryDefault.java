package ua.ivan909020.bot.repositories.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ua.ivan909020.bot.domain.entities.Category;
import ua.ivan909020.bot.domain.entities.Product;
import ua.ivan909020.bot.repositories.ProductRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryDefault implements ProductRepository {

    private final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();

    private final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/shop_telegram_bot";
    private final String username = "postgres";
    private final String password = "postgres";

    @Override
    public Product findById(Integer productId) {
        Session session = sessionFactory.openSession();
        Product product = session.get(Product.class, productId);
        session.close();
        return product;
    }

    @Override
    public List<Product> findAllByCategoryName(String categoryName, int offset, int size) {
        Session session = sessionFactory.openSession();
        List<Product> products = session.createQuery("from Product as products where category.name = :categoryName order by products.rating desc, products.name asc", Product.class)
                .setParameter("categoryName", categoryName)
                .setFirstResult(offset)
                .setMaxResults(size)
                .getResultList();
        session.close();
        return products;
    }

    @Override
    public void update(Product product)
    {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(product);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Product> findClientsChoice() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        List<Product> products = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;

        String query = "SELECT products.id, products.category_id, categories.name, products.photo_url, products.name, products.description, products.price, products.rating, COUNT(*) " +
                "FROM products, order_items, categories " +
                "WHERE order_items.product_id = products.id AND order_items.order_id IN (SELECT id FROM orders WHERE status LIKE 'COMPLETED') " +
                "AND products.category_id = categories.id " +
                "GROUP BY products.id, categories.name " +
                "HAVING products.rating > 4 AND COUNT(order_items.order_id) > 1 " +
                "ORDER BY COUNT(order_items.order_id) DESC, products.rating DESC " +
                "FETCH FIRST 10 ROWS ONLY;";

        try {
            connection = DriverManager.getConnection(DB_URL, username, password);
            statement = connection.createStatement();

            if (connection != null) {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    Product product = new Product();
                    product.setId(rs.getInt(1));
                    Category category = new Category();
                    category.setId(rs.getInt(2));
                    category.setName(rs.getString(3));
                    product.setCategory(category);
                    product.setPhotoUrl(rs.getString(4));
                    product.setName(rs.getString(5));
                    product.setDescription(rs.getString(6));
                    product.setPrice(rs.getFloat(7));
                    product.setRating(rs.getFloat(8));

                    products.add(product);
                }
            } else {
                System.out.println("Failed to make connection to database");
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
        }
        return products;
    }


}
