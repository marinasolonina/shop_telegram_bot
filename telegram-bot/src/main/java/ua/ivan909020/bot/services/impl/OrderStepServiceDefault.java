package ua.ivan909020.bot.services.impl;

import ua.ivan909020.bot.commands.Command;
import ua.ivan909020.bot.commands.Commands;
import ua.ivan909020.bot.domain.entities.Order;
import ua.ivan909020.bot.exceptions.ValidationException;
import ua.ivan909020.bot.repositories.OrderStepRepository;
import ua.ivan909020.bot.repositories.impl.OrderStepRepositoryDefault;
import ua.ivan909020.bot.services.ClientService;
import ua.ivan909020.bot.services.OrderStepService;

import java.util.Map;

public class OrderStepServiceDefault implements OrderStepService {

    private static final OrderStepService INSTANCE = new OrderStepServiceDefault();

    private final OrderStepRepository repository = new OrderStepRepositoryDefault();

    private final ClientService clientService = ClientServiceDefault.getInstance();

    private OrderStepServiceDefault() {
    }

    public static OrderStepService getInstance() {
        return INSTANCE;
    }

    @Override
    public void revokeOrderStep(Long chatId) {
        repository.setOrderStepNumber(chatId, 0);
    }

    @Override
    public void previousOrderStep(Long chatId) {
        int orderStep = repository.findOrderStepNumberByChatId(chatId);
        String action = clientService.findActionByChatId(chatId);
        Map<Integer, Command<Long>> orderSteps = repository.getOrderSteps();
        if (orderStep > 1) {
            if (this.findCachedOrderByChatId(chatId).getDeliveryMethod().equals(Commands.DELIVERY_METHOD_COMMAND_COURIER) && action.equals("order=enter-client-city"))
            {
                orderStep -= 3;
            }
            else if (this.findCachedOrderByChatId(chatId).getDeliveryMethod().equals(Commands.DELIVERY_METHOD_COMMAND_ORDER_A_TABLE) && action.equals("time=set-time")){
                orderStep -= 3;
            }
            else if (this.findCachedOrderByChatId(chatId).getDeliveryMethod().equals(Commands.DELIVERY_METHOD_COMMAND_PICKUP) && action.equals("time=set-time")) {
                orderStep -= 5;
            }
            else {
                orderStep --;
            }

            repository.setOrderStepNumber(chatId, orderStep);
            orderSteps.get(orderStep).execute(chatId);
        }
    }

    @Override
    public void nextOrderStep(Long chatId) {
        int orderStep = repository.findOrderStepNumberByChatId(chatId);
        Map<Integer, Command<Long>> orderSteps = repository.getOrderSteps();
        if (orderStep < orderSteps.size()) {
            orderStep++;
            repository.setOrderStepNumber(chatId, orderStep);
            orderSteps.get(orderStep).execute(chatId);
        }
    }

    @Override
    public void skipAddress(Long chatId) {
        int orderStep = repository.findOrderStepNumberByChatId(chatId);
        Map<Integer, Command<Long>> orderSteps = repository.getOrderSteps();
        if (orderStep < orderSteps.size()) {
            orderStep += 3;
            repository.setOrderStepNumber(chatId, orderStep);
            orderSteps.get(orderStep).execute(chatId);
        }
    }

    @Override
    public void skipAll(Long chatId) {
        int orderStep = repository.findOrderStepNumberByChatId(chatId);
        Map<Integer, Command<Long>> orderSteps = repository.getOrderSteps();
        if (orderStep < orderSteps.size()) {
            orderStep += 5;
            repository.setOrderStepNumber(chatId, orderStep);
            orderSteps.get(orderStep).execute(chatId);
        }
    }

    @Override
    public void skipPersonsCount(Long chatId) {
        int orderStep = repository.findOrderStepNumberByChatId(chatId);
        Map<Integer, Command<Long>> orderSteps = repository.getOrderSteps();
        if (orderStep < orderSteps.size()) {
            orderStep += 3;
            repository.setOrderStepNumber(chatId, orderStep);
            orderSteps.get(orderStep).execute(chatId);
        }
    }

    @Override
    public Order findCachedOrderByChatId(Long chatId) {
        if (chatId == null) {
            throw new IllegalArgumentException("ChatId of Client should not be NULL");
        }
        return repository.findCachedOrderByChatId(chatId);
    }

    @Override
    public void saveCachedOrder(Long chatId, Order order) {
        if (order == null) {
            throw new IllegalArgumentException("Order should not be NULL");
        }
        if (order.getClient() == null) {
            throw new ValidationException("Client should not be NULL");
        }
        repository.saveCachedOrder(chatId, order);
    }

    @Override
    public void updateCachedOrder(Long chatId, Order order) {
        if (order == null) {
            throw new IllegalArgumentException("Order should not be NULL");
        }
        if (order.getClient() == null) {
            throw new ValidationException("Client should not be NULL");
        }
        repository.updateCachedOrder(chatId, order);
    }

    @Override
    public void deleteCachedOrderByChatId(Long chatId) {
        if (chatId == null) {
            throw new IllegalArgumentException("ChatId of Client should not be NULL");
        }
        repository.deleteCachedOrderByChatId(chatId);
    }

}
